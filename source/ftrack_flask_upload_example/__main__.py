# :coding: utf-8
# :copyright: Copyright (c) 2015 ftrack

import argparse
import logging
import sys
import tempfile

import ftrack_flask_upload_example.server


def main(arguments=None):
    '''ftrack flask upload example.'''
    if arguments is None:
        arguments = []

    parser = argparse.ArgumentParser()

    # Allow setting of logging level from arguments.
    loggingLevels = {}
    for level in (
        logging.NOTSET, logging.DEBUG, logging.INFO, logging.WARNING,
        logging.ERROR, logging.CRITICAL
    ):
        loggingLevels[logging.getLevelName(level).lower()] = level

    parser.add_argument(
        '-v', '--verbosity',
        help='Set the logging output verbosity.',
        choices=loggingLevels.keys(),
        default='info'
    )

    parser.add_argument(
        '-d', '--directory',
        help='Set path to directory to store files in.',
        default=tempfile.gettempdir()
    )

    parser.add_argument(
        '-p', '--port',
        help='Set port to use.',
        default=5001
    )

    parser.add_argument(
        '--host',
        help=(
            'Set this to "0.0.0.0" to have the server available externally '
            'as well. Defaults to "127.0.0.1"'
        ),
        default='127.0.0.1'
    )

    namespace = parser.parse_args(arguments)

    logging.basicConfig(level=loggingLevels[namespace.verbosity])

    ftrack_flask_upload_example.server.run(
        host=namespace.host, port=int(namespace.port),
        directory=namespace.directory
    )


if __name__ == '__main__':
    raise SystemExit(
        main(sys.argv[1:])
    )
