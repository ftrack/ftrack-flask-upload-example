..
    :copyright: Copyright (c) 2015 ftrack

.. _using:

*****
Using
*****

To run the example flask server simply start it by running::

    python -m ftrack_flask_upload_example

To see available options the server can be started with run it with the
`help` option::
    
    >>> python -m ftrack_flask_upload_example --help
    usage: __main__.py [-h] [-v {info,warning,critical,error,debug,notset}]
                   [-d DIRECTORY] [-p PORT] [--host HOST]

    optional arguments:
        -h, --help      show this help message and exit
        -v {info,warning,critical,error,debug,notset}, --verbosity {info,warning,critical,error,debug,notset}
                        Set the logging output verbosity.
        -d DIRECTORY, --directory DIRECTORY
                        Set path to directory to store files in.
        -p PORT, --port PORT  Set port to use.
        --host HOST     Set this to "0.0.0.0" to have the server available
                        externally as well. Defaults to "127.0.0.1"